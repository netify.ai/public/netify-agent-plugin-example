// Netify Agent Plugin Example
// Copyright (C) 2015-2022 eGloo Incorporated <http://www.egloo.ca>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _ND_PLUGIN_EXAMPLE_H
#define _ND_PLUGIN_EXAMPLE_H

class ndPluginExampleDetection : public ndPluginDetection
{
public:
    ndPluginExampleDetection(const string &tag);
    virtual ~ndPluginExampleDetection();

    virtual void *Entry(void);

    virtual void ProcessFlow(ndDetectionEvent event, ndFlow *flow);

    virtual void GetVersion(string &version) { version = PACKAGE_VERSION; }
};

class ndPluginExampleStats : public ndPluginStats
{
public:
    ndPluginExampleStats(const string &tag);
    virtual ~ndPluginExampleStats();

    virtual void *Entry(void);

    virtual void ProcessStats(ndStatsEvent event);
    virtual void ProcessStats(const ndInterfaces &nd_interfaces);
    virtual void ProcessStats(const ndPacketStats &pkt_totals);
    virtual void ProcessStats(
        const string &iface, const ndPacketStats &pkt_stats);
    virtual void ProcessStats(const ndFlowMap *flows);

    virtual void GetVersion(string &version) { version = PACKAGE_VERSION; }
};

#endif // _ND_PLUGIN_EXAMPLE_H
// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
